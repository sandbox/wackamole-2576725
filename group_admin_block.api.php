<?php

/**
 * @file
 * Hooks provided by the group admin block module.
 */

/**
 * Alter groups admin block.
 *
 * @param array $vars
 *   The group admin block render array.
 */
function hook_group_admin_block_alter(array &$vars) {
  // Reorder links.
  $vars['content']['invite_new_member']['#weight'] = 1;
  $vars['content']['manage_members']['#weight'] = 2;
  $vars['content']['edit_group']['#weight'] = 3;
}
