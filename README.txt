Group Admin Block

-------------------------------------------------------------------------------
ABOUT
-------------------------------------------------------------------------------
This module creates a block, which displays a list of links to admin tasks for
a Group, for a Group administrator. The Group context must be set to the
group you want the links to relate to on the page(s) the block is displayed.

Links included:
- Edit the group,
- Manage members,
- Invite new member,
- Manage menus (if group_menu is enabled),
- Links to add each type of Group Content.

An alter hook is provided for adding additional links, reordering links and
general theming.

-------------------------------------------------------------------------------
INSTALLATION
-------------------------------------------------------------------------------
When you enable the module, you'll get a new a new block.

A Group context will have to be active for the block to appear.

You can add further items, or reorder the link by implementing
hook_group_admin_block_alter() - for examples see group_admin_block.api.php.
